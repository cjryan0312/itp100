# Base-64 Experiment Notes
## By: Christopher Ryan

### How do we use this file?
Idea 1: we did vi into one of the files and going to attempt to run it
did not work

Idea 2: Run a python3 shell and run it in there to try and get it to run 
did not work 

Idea 3: Run a python 3 shell on the base 64 tools file and get it to run there 
nothing happened 

Idea 4: Try running in shell and type a function name with parameters to get it to work 
syntax error 

Idea 5(after fix): ./ base64whatever filename 
It worked! it just made a new file 

### Notes:
base64 can convert ascii, chinese characters, and cursed text, very cool!



