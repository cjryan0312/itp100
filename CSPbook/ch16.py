#1
"""
>>> print(len(combined))
15
>>> print(combined)
My name is Mark
>>> print(name * 3)
MarkMarkMark
"""
name = "Mark"
start = "My name is "
combined = start + name
print(len(combined))
print(combined)
print(name * 3)


#2
"""
>>> print(len(c_string))
14
"""
a_string = "limo"
b_string = "bow"
c_string = (a_string + b_string) * 2
print(len(c_string))

#3
"""
>>> print(len(my_first_list))
3
>>> print(my_first_list * 3)
[12, 'ape', 13, 12, 'ape', 13, 12, 'ape', 13]
>>> print(my_second_list)
[12, 'ape', 13, 321.4]
"""
my_first_list = [12,"ape",13]
print(len(my_first_list))
print(my_first_list * 3)
my_second_list = my_first_list + [321.4]
print(my_second_list)


#4
def name_procedure(name):
    """
    >>> name_prodecure("John")
    My name is JohnJohn
    19
    """
    start = "My name is "
    combined = start + (name * 2)
    print(combined)
    print(len(combined))    
name_procedure("John") 

#5
def item_lister(items):
    """
    >>> item_lister([2, 4, 6, 8])
    ['First item', 'First item', 7, 8]
    """
    items[0] = "First item"
    items[1] = items[0]
    items[2] = items[2] + 1
    print(items)
item_lister([2, 4, 6, 8])

#6
def grade_average(a_list):
    """
    >>> grade_average([99, 100, 74, 63, 100, 100])
    89.3333333333
    """
    sum = 0
    for num in a_list:
        sum = sum + num
        average = sum / len(a_list)
    return average

a_list = [99, 100, 74, 63, 100, 100]
print(grade_average(a_list))

#7
"""
>>> print(so_far)
['This']
['is', 'This']
['a', 'is', 'This']
['list', 'a', 'is', 'This']
"""
source = ["This", "is", "a", "list"]
so_far = []
for index in range(0,len(source)):
    so_far = [source[index]] + so_far
    print(so_far)

#8
"""
>>> print(items)
['hihi', 0, 0, 4]
"""
items = ["hi", 2, 3, 4]
items[0] = items[0] + items[0]
items[1] = items[2] - 3
items[2] = items[1]
print(items)

#9
"""
>>> print(so_far)
['list', 'a', 'is', 'This']
"""
# setup the source list
source = ["This","is", "a","list"]

# Set the accumulator to the empty list
so_far = []

# Loop through all the items in the source list
for index in range(0, len(source)):

    # Add the current item in the source and print the current items in so_far
    so_far = [source[index]] + so_far
print(so_far)

#10
"""
>>> print(so_far)
['list', 'a', 'is', 'This', 'This', 'is', 'a', 'list']
"""
# setup the source list
source = ["This","is","a","list"]

# Set the accumulator to the empty list
so_far = []

# Loop through all the items in the source list
for index in range(0,len(source)):

    # Add a list with the current item from source to so_far
    so_far = [source[index]] + so_far
    so_far = so_far + [source[index]]
    
print(so_far)

#11
"""
>>> print(even_list)
[0, 2, 4, 6, 8, 10]
"""
numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
even_list = []
for index in range(0,len(numbers),2):
    even_list = even_list + [numbers[index]]
print(even_list)


#12
"""
>>> print(numbers)
[0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50]
"""
# initialize the variables
numbers = []
evens = []

# loop though every other index
for index in range(0, 51, 5):
    # add the lists
    numbers = numbers + [index]

# print the result
print(numbers)



#13
def countdown(starter):
    """
    >>> countdown(5)
    5
    4
    3
    2
    1
    0
    """
    for index in range(starter, -1, -1):
        print(index) 
countdown(5)

#14
"""
>>> print(new_list)
['This', 'is', 'a', 'list']
"""
# initialize the variables
source = ["This", "is", "a", "list"]
new_list = []

# loop from the last index to the first (0)
for index in range(3, -1, -1):
# append the lists
    new_list = [source[index]] + new_list

# print the current value of the list
print(new_list)


#15
def getOddIndicesList(numbers):
    """
    >>> getOddIndicesList([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    [1, 3, 5, 7, 9]
    """
    evenList = []
    for index in range(1,len(numbers),2):
        evenList = evenList + [numbers[index]]
    return(evenList)

print(getOddIndicesList([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))


#16
def adding(numbers):
    """
    >>> adding([1, 2, 3, 4])
    6
    7
    8
    9
    """
    addList = []
    for index in range(0, len(numbers)):
        addList = numbers[index] + 5
        print(addList)
    return addList 

adding([1, 2, 3, 4])

#17
def sumPos(theList):
    """
    >>> sumPos([-3, 2, -8, 5, -20, -33, 15])
    22
    """
    sum = 0
    for item in theList:
        if item >= 0:
            sum = sum + item
    return sum

print(sumPos([-3, 2, -8, 5, -20, -33, 15]))

#18
def sumAll(numbers):
    """
    >>> sumAll([-3, 2, -8, 5, -20, -33, 15])
    86
    """
    sumPos = 0
    sumNeg = 0
    total = 0
    for item in numbers:
        if item >= 0:
            sumPos = sumPos + item
        if item < 0:
            item = abs(item)
            sumNeg = sumNeg + item
    total = sumPos + sumNeg
    return(total)

print(sumAll([-3, 2, -8, 5, -20, -33, 15]))
    
#19
def reverseEveryOther(theList):
    """
    >>> reverseEveryOther([0,1,2,3,4,5])
    [5, 3, 1]
    """
    newList = []
    for index in range(len(theList) - 1, -1, -2):
        newList = newList + [theList[index]]
    return newList


print(reverseEveryOther([0,1,2,3,4,5]))

#20
def adding(num):
    """
    >>> adding(10)
    15
    """
    newList = []
    everyOther = []
    total = 0
    for index in range(1, num, 2):
        newList = newList + [index]
    
    for index in range(0, len(newList), 2):
        everyOther = everyOther + [newList[index]]
    for index in range(0, len(everyOther)):
        total = everyOther[index] + total
    return total

print(adding(10))


if __name__ == '__main__':
    import doctest
    doctest.testmod()


