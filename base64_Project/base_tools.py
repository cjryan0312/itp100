def to_base(num, base):
    """
    Convert a decimal integer into a string representation of the digits
    representing the number in the base (between 2 and 10) provided.

      >>> to_base(10, 3)
      '101'
      >>> to_base(11, 2)
      '1011'
      >>> to_base(10, 6)
      '14'
      >>> to_base(21, 3)
      '210'
      >>> to_base(21, 11)
      '1A'
      >>> to_base(47, 16)
      '2F'
      >>> to_base(65535, 16)
      'FFFF'
      >>> to_base(33, 64)
      'h'
      >>> to_base(9, 64)
      'J'
      >>> to_base(54, 64) 
      '2'
    """
    if num == 0:
        return '0'
    
    answer = ""
    
    digits = '0123456789ABCDEF'    
    base64_digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    
    if base == 64:
        digits = base64_digits
        
    while num:
        answer = digits[num % base] + answer
        num //= base 
    
    return answer 

def base64decode(four_chars):
    """
      >>> base64decode('STOP')
      b'I3\\x8F'
      >>>
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    ints = [digits.index(ch) for ch in four_chars]
    b1 = (ints[0] << 2) | ((ints[1] & 48) >> 4)
    b2 = (ints[1] & 15) << 4 | ints[2] >> 2
    b3 = (ints[2] & 3) << 6 | ints[3] 
    
    return bytes([b1, b2, b3])
    
def base64encode(tre_bytes):
     # Base64 Digits
    digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
    # Take string, remove padding
    test_str = str(ys).replace('=', "")
    # Split the string into characters
    strlist = [char for char in test_str]
    # Create the blank variables
    bins, o, fo = "", [], []
    # Go through the characters in the word.
    for s in strlist:
        # Find the decimal number for the digit
        loc = digits.find(s)
        # Turn the decimal number into a binary representation
        loc = format(loc, 'b')
        # This bit just adds the zeros to the beginning that got trimmed off
        if len(loc) < 6:
            loc = str(loc)
            loc = ("0" * (6 - len(loc))) + loc
        # Then you create a string of all the binary values
        bins = bins + loc
    # This splits the string of binary values into a list with each item being 8 digits (binary for ascii)
    while bins:
        o.append(bins[:8])
        bins = bins[8:]
    # This removes the leftover item that isn't a letter (The 0's added to the end by Base64)
    for s in o:
        if len(s) == 8:
            fo.append(s)
    # This turns all of the binary numbers into ascii characters and then makes them a string.
    fs = ''.join([chr(int(binary, 2)) for binary in fo])
    return fs

def to_binary(num):
    """
    Convert a decimal integer into a string representation of its binary
    (base2) digits.

      >>> to_binary(10) 
      '1010'
      >>> to_binary(12) 
      '1100'
      >>> to_binary(0) 
      '0'
      >>> to_binary(1) 
      '1'
    """
    return to_base(num, 2)

def to_base3(num):
    """
    Convert an decimal integer into a string representation of its base3
    digits.

      >>> to_base3(10)
      '101'
      >>> to_base3(12)
      '110'
      >>> to_base3(6)
      '20'
    """
    finished = False
    divBy = num
    answer = ""
    while finished == False:
        modulo = divBy % 3
        divBy = divBy // 3
        if divBy == 0:
            answer = str(modulo) + answer
            finished = True
        else:
            answer = str(modulo) + answer 
    return answer

def to_base4(num):
    """
    Convert an decimal integer into a string representation of its base4
    digits.

      >>> to_base4(20)
      '110'
      >>> to_base4(28)
      '130'
      >>> to_base4(3)
      '3'
    """
    return to_base(num, 4) 


if __name__ == '__main__':
    import doctest
    doctest.testmod()
