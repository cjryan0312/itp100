def only_evens(nums):
    """
      >>> only_evens([3, 8, 5, 4, 12, 7, 2])
      [8, 4, 12, 2]
      >>> my_nums = [4, 7, 19, 22, 42]
      >>> only_evens(my_nums)
      [4, 22, 42]
      >>> my_nums
      [4, 7, 19, 22, 42]
    """
    my_nums = [4, 7, 19, 22, 42] #this is a list tested in doctest so we make it here
    our_evens = [] #this will be the list that contains the even numbers 
    for index in range(len(nums)): 
        if nums[index] % 2 == 0: #if the number in the location of the list divisible by 2 it evaluates
            our_evens += [nums[index]] #if the number is divisible it is added to only_evens
    return our_evens 

def num_even_digits(n):
    """
      >>> num_even_digits(123456)
      3
      >>> num_even_digits(2468)
      4
      >>> num_even_digits(1357)
      0
      >>> num_even_digits(2)
      1
      >>> num_even_digits(20)
      2
    """
    total = 0 #setting total to 0
    stringN = str(n) #making the number that is input a string
    nList = [] #making a list for the numbers empty 
    for index in range(len(stringN)):
        splice = stringN[index:index + 1] #setting splice to the string splice of that digit 
        splice = int(splice) #setting the splice to an integer
        nList.append(splice) #adding that number to the list 

    for item in range(len(nList)):
        if nList[item] % 2 == 0: #checking if the number is divisible by 2 and it evaluates 
            total += 1 #if it does evaluate by 2 it adds to the total 
    return total 

def sum_of_squares_of_digits(nn):
    """
      >>> sum_of_squares_of_digits(1)
      1
      >>> sum_of_squares_of_digits(9)
      81
      >>> sum_of_squares_of_digits(11)
      2
      >>> sum_of_squares_of_digits(121)
      6
      >>> sum_of_squares_of_digits(987)
      194
    """
    total = 0 #setting total to 0
    stringN = str(nn) #making a string version of the argument "nn"
    nList = [] #making empty list nList
    for index in range(len(stringN)):
        splice = stringN[index:index + 1] #splices each digit in the string into var splice
        splice = int(splice) #makes the splice we took an integer 
        nList.append(splice) #puts the splice into the list
    
    for item in range(len(nList)):
        total += nList[item] * nList[item] #multiplies the number by itself(squaring) and adds to total
    return total 

def lots_of_letters(word):
    """
      >>> lots_of_letters('Lidia')
      'Liidddiiiiaaaaa'
      >>> lots_of_letters('Python')
      'Pyyttthhhhooooonnnnnn'
      >>> lots_of_letters('')
      ''
      >>> lots_of_letters('1')
      '1'
    """
    wList = [] #makes empty list wList
    stringW = "" #make blank string stringW
    for index in range(len(word)):
        splice = word[index : index + 1] #takes a splice of every string in "word"
        wList.append(splice) #puts the splice into the list 
    
    for item in range(len(word)):
        wList[item] = wList[item] * (int(item) + 1) #takes each letter in list and multiplies it by pos
        stringW += wList[item] #stringW adds each item in wList to the list
    return stringW

def gcf(m, n):
    """
      >>> gcf(10, 25)
      5
      >>> gcf(8, 12)
      4
      >>> gcf(5, 12)
      1
      >>> gcf(24, 12)
      12
    """
    divBy = 1 #sets divBy to 1
    gcfList = [1] #makes gcfList with only 1 in it 
    while divBy <= m or divBy <= n:
        mModulo = m % divBy #modulo the argument by divBy for divisible check
        nModulo = n % divBy #^^^
        if mModulo == 0 and nModulo == 0: #if both are divisible by the number it evaluates
            gcfList[0] = divBy #the 1 item in the list is now equal to the number moduloed by 
            divBy += 1 #divBy is added by 1 to continue the code 
        else:
            divBy += 1 #added by 1 to continue and no infinite error 
    return int(gcfList[0])

def is_prime(n):
    """
        >>> is_prime(7)
        True
        >>> is_prime(25)
        False
        >>> is_prime(1)
        False
        >>> is_prime(2)
        True
    """
    moduloCheck = 0 #sets moduloCheck
    divideBy = 1 #sets divideBy to 1
    if n != 2: #assuming the number isn't 2(which is a prime number but its scuffed) it runs
        while divideBy <= n:
            answer = n % divideBy #modulo the argument to see if it can be divided by anything 
            if answer == 0: #if it is this evaluates
                moduloCheck += 1 #adds 1 to moduloCheck 
            divideBy += 1 #added by 1 to continue and no infinite error 
        
        if moduloCheck == 2: #after the while loop is finished if there r only 2 divisibles this goes off
            return True #if only 2 divisibles it is a prime number 
        else:
            return False #if more/only 1 divisible it is not a prime number
    else: #if it is 2 it auto evaluates as True 
        return True



if __name__ == '__main__':
    import doctest
    doctest.testmod()
