from gasp import *

#challenge 3
#def moan():
    #print("Python is uselss!")
    #print("And so are these worksheets")
#moan()

#challenge 4
#def times(x):
    #print(x, "*", x, "is", x*x)
#times(7)

begin_graphics()

def face(x, y):
    #head
    Circle((300, 220), 40) 

    #eyes 
    for xx in x - 15, x + 15:
        Circle((xx, 235), 5)
        Circle((xx, 235), 5)

    #schnozzer
    Line((x - 10, y - 10),(x, y + 10))
    Line((x - 10, y - 10),(x + 10, y - 10))

    #eyebrows 
    Line((x - 25, y + 20),(x - 5, y - 20))
    Line((x + 5, y + 20),(x + 25, y + 20)) 

    #mouth  
    Arc((x, y), 30, 225, 315)

face(300, 220)

update_when('key_pressed')                             
end_graphics()

