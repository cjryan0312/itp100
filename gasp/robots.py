from gasp import *
#challenge 3
#begin_graphics()
#ball_x = 5
#ball_y = 5
#c = Circle((ball_x, ball_y), 5)
#while ball_x < 635:
    #sleep(0.02)
    #ball_x += 4
    #ball_y += 3
    #move_to(c, (ball_x, ball_y))
#end_graphics()

begin_graphics()         
finished = False

def place_player():
    global player_x, player_y, player_shape
    player_x = random_between(0, 63)
    player_y = random_between(0, 47)

def collided():
    if player_y == robot_y and player_x == robot_x:
        return True
    else:
        return False

def safely_place_player():
    place_player()
    global player_x, player_y, player_shape

    while collided():
        place_player()
        player_shape = Circle((10* player_x + 5, 10 * player_y + 5), 5, filled=True)

def safely_place_player():
    place_player()
    global player_x, player_y, player_shape

    while collided():
        place_player()

def place_robot():
    global robot_x, robot_y, robot_shape
    robot_x = random_between(0, 63)
    robot_y = random_between(0, 47)
    robot_shape = Box((10*robot_x, 10*robot_y), 8, 8)

def move_player():
    global player_x, player_y, player_shape
    
    key = update_when('key_pressed')
    #random
    if key == '5':
        player_x = random_between(0, 63)
        player_y = random_between(0, 47)

    #right
    elif key == '6' and player_x < 63:
        player_x += 1
    #diagonal bottom right
    elif key == '3':
        if player_x < 63:
            player_x += 1
        if player_y > 0:
            player_y -= 1
    #diagonal top right
    elif key == '9':
        if player_x < 63:
            player_x += 1
        if player_y < 47:
            player_y += 1
    #left
    elif key == '4' and player_x > 0:
        player_x -= 1
    #diagonal top left 
    elif key == '7':
        if player_x > 0:
            player_x -= 1
        if player_y < 47:
            player_y += 1
    #diagonal bottom right 
    elif key == '1':
        if player_x > 0:
            player_x -= 1
        if player_y > 0:
            player_y -= 1
    #up
    elif key == '8' and player_y < 47:
        player_y += 1
    #down
    elif key == '2' and player_y > 0:
        player_y -= 1

    move_to(player_shape, (10*player_x+5, 10*player_y+5))

def move_robot():
    global player_y, player_x, robot_x, robot_y, robot_shape
    
    #player is right of robot
    if player_x > robot_x:
        if player_y > robot_y:
            robot_y += 1
            robot_x += 1
        elif player_y < robot_y:
            robot_y -= 1
            robot_x += 1
        elif player_y == robot_y:
            robot_x += 1

    #player is left of robot
    elif player_x < robot_x:
        if player_y > robot_y:
            robot_y += 1
            robot_x -= 1
        elif player_y < robot_y:
            robot_y -= 1
            robot_x -= 1
        elif player_y == robot_y:
            robot_x -= 1

    elif player_x == robot_x:
        if player_y > robot_y:
            robot_y += 1
        elif player_y < robot_y:
            robot_y -= 1

    move_to(robot_shape, (10 * robot_x, 10 * robot_y))


def check_collisions():
    if collided() == True:
        finished = True
        words = "You've been caught!" 
        message = Text(words, (320, 240), color=color.BLACK, size=15)
        sleep(3) 

place_robot()
safely_place_player()

while not finished:
    move_player()
    move_robot()
    check_collisions()

end_graphics()              
